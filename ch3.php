<?php
        //Enter your code here, enjoy!
class ShopProduct
{
    public $title = "Название";
    public $firstName = "Михаил";
    public $lastName = "Булгаков";
    public $price = 0;
    
    public function __construct($title, $firstName, $lastName, $price)
    {
        $this->title = $title;
        $this->firstName = $firstName;
        $this->lastName = $lastName;
        $this->price = $price;
    }
    
    public function getProductName()
    {
        return $this->firstName." ".$this->lastName;
    }
}

$product = new ShopProduct("Title", "Alex", "Asd", 50.0);
echo $product->getProductName();